package technologies.android.tawkmobileassesment.test

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.gson.Gson
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler.ExecutorWorker
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import technologies.android.tawkmobileassesment.core.data.db.AppDatabase
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.NoteData
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.extension.fromJson
import technologies.android.tawkmobileassesment.core.extension.getOrAwaitValue
import technologies.android.tawkmobileassesment.core.network.repo.UsersRepository
import technologies.android.tawkmobileassesment.feature.dashboard.user_details.UserDetailsViewModel
import java.lang.Exception
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


@RunWith(MockitoJUnitRunner::class)
class UserDetailsViewModelTest {

    lateinit var repo: UsersRepository
    lateinit var userDataDao: UserDataDao
    lateinit var notesDao: NoteDataDao
    lateinit var vm: UserDetailsViewModel
    lateinit var gson: Gson

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val immediateScheduler: Scheduler = object : Scheduler() {

        override fun createWorker() = ExecutorWorker(Executor { it.run() }, true)

        // This prevents errors when scheduling a delay
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            return super.scheduleDirect(run, 0, unit)
        }

    }

    @Before
    fun setup() {
        RxJavaPlugins.setIoSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { immediateScheduler }

        repo = mockk(relaxed = true)
        userDataDao = mockk(relaxed = true)
        notesDao = mockk(relaxed = true)
        vm = UserDetailsViewModel(repo, userDataDao, notesDao)
        gson = Gson()
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }


    @Test fun successGetUserDetails(){
        vm.apply {

            val userDataResponse: UserData = gson.fromJson("{ \"login\": \"mojombo\", \"id\": 1, \"node_id\": \"MDQ6VXNlcjE=\", \"avatar_url\": \"https://avatars0.githubusercontent.com/u/1?v=4\", \"gravatar_id\": \"\", \"url\": \"https://api.github.com/users/mojombo\", \"html_url\": \"https://github.com/mojombo\", \"followers_url\": \"https://api.github.com/users/mojombo/followers\", \"following_url\": \"https://api.github.com/users/mojombo/following{/other_user}\", \"gists_url\": \"https://api.github.com/users/mojombo/gists{/gist_id}\", \"starred_url\": \"https://api.github.com/users/mojombo/starred{/owner}{/repo}\", \"subscriptions_url\": \"https://api.github.com/users/mojombo/subscriptions\", \"organizations_url\": \"https://api.github.com/users/mojombo/orgs\", \"repos_url\": \"https://api.github.com/users/mojombo/repos\", \"events_url\": \"https://api.github.com/users/mojombo/events{/privacy}\", \"received_events_url\": \"https://api.github.com/users/mojombo/received_events\", \"type\": \"User\", \"site_admin\": false, \"name\": \"Tom Preston-Werner\", \"company\": null, \"blog\": \"http://tom.preston-werner.com\", \"location\": \"San Francisco\", \"email\": null, \"hireable\": null, \"bio\": null, \"twitter_username\": null, \"public_repos\": 62, \"public_gists\": 62, \"followers\": 22089, \"following\": 11, \"created_at\": \"2007-10-20T05:24:19Z\", \"updated_at\": \"2020-08-27T21:48:33Z\" }")

            every { repo.getUserDetails("mojombo") } returns
                    Maybe.just(userDataResponse)

            every {
                userDataDao.insert(userDataResponse)
            } returns Completable.complete()

            hasInternetConnection.value = true
            getUserDetails("mojombo")

            assert(userDetails.getOrAwaitValue().login.isNotEmpty())
            assert(userDetails.getOrAwaitValue().name?.isNotEmpty() == true)
            assert(userDetails.getOrAwaitValue().name == "Tom Preston-Werner")
        }
    }

    @Test fun errorGetUserDetails(){
        vm.apply {

            val errorMessage = "User not found"

            every { repo.getUserDetails("mojombo") } returns
                    Maybe.error(Exception(errorMessage))

            hasInternetConnection.value = true
            getUserDetails("mojombo")

            assert(ui.getOrAwaitValue().message?.first == errorMessage)
        }
    }

    @Test fun successSaveNote(){
        vm.apply {

            val notes = "Sample"

            val userDataResponse: UserData = gson.fromJson("{ \"login\": \"mojombo\", \"id\": 1, \"node_id\": \"MDQ6VXNlcjE=\", \"avatar_url\": \"https://avatars0.githubusercontent.com/u/1?v=4\", \"gravatar_id\": \"\", \"url\": \"https://api.github.com/users/mojombo\", \"html_url\": \"https://github.com/mojombo\", \"followers_url\": \"https://api.github.com/users/mojombo/followers\", \"following_url\": \"https://api.github.com/users/mojombo/following{/other_user}\", \"gists_url\": \"https://api.github.com/users/mojombo/gists{/gist_id}\", \"starred_url\": \"https://api.github.com/users/mojombo/starred{/owner}{/repo}\", \"subscriptions_url\": \"https://api.github.com/users/mojombo/subscriptions\", \"organizations_url\": \"https://api.github.com/users/mojombo/orgs\", \"repos_url\": \"https://api.github.com/users/mojombo/repos\", \"events_url\": \"https://api.github.com/users/mojombo/events{/privacy}\", \"received_events_url\": \"https://api.github.com/users/mojombo/received_events\", \"type\": \"User\", \"site_admin\": false, \"name\": \"Tom Preston-Werner\", \"company\": null, \"blog\": \"http://tom.preston-werner.com\", \"location\": \"San Francisco\", \"email\": null, \"hireable\": null, \"bio\": null, \"twitter_username\": null, \"public_repos\": 62, \"public_gists\": 62, \"followers\": 22089, \"following\": 11, \"created_at\": \"2007-10-20T05:24:19Z\", \"updated_at\": \"2020-08-27T21:48:33Z\" }")

            every { repo.getUserDetails("mojombo") } returns
                    Maybe.just(userDataResponse)

            every {
                userDataDao.insert(userDataResponse)
            } returns Completable.complete()

            every {
                notesDao.insert(NoteData(userDataResponse.id, notes))
            } returns Completable.complete()

            hasInternetConnection.value = true
            getUserDetails("mojombo")

            onNotesChange(notes)

            assert(ui.getOrAwaitValue().notesValid)

            saveNote()

            assert(ui.getOrAwaitValue().message?.first == "Notes saved successfully!")
        }
    }

    @Test fun invalidNote() {
        vm.apply {
            onNotesChange("")

            assert(!ui.getOrAwaitValue().notesValid)
        }
    }


}