package technologies.android.tawkmobileassesment.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import technologies.android.tawkmobileassesment.core.di.scope.ActivityScope
import technologies.android.tawkmobileassesment.feature.dashboard.DashboardActivity
import technologies.android.tawkmobileassesment.feature.dashboard.user_details.UserDetailsActivity

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsActivity(): UserDetailsActivity
}