package technologies.android.tawkmobileassesment.core.data.datasource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.network.repo.UsersRepository
import java.util.logging.Logger

class UsersDataSource(private val repository: UsersRepository,
                      private val userDao: UserDataDao,
                      private val loading: MutableLiveData<Boolean>,
                      private val empty: MutableLiveData<Boolean>,
                      private val hasInternet: MutableLiveData<Boolean>) : ItemKeyedDataSource<Int, UserData>() {

    private var disposables = CompositeDisposable()
    private var pageNumber = 0
    private var size = 5

    var params: LoadParams<Int>? = null
    var callback: LoadCallback<UserData>? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<UserData>
    ) {
        if(hasInternet.value == true) {
             repository.getUsers(pageNumber)
                .doOnSubscribe { loading.postValue(true) }
                .subscribeWith(object : DisposableMaybeObserver<List<UserData>>() {

                    override fun onComplete() {
                        loading.postValue(false)
                    }

                    override fun onSuccess(users: List<UserData>) {
                        onUsersFetched(users, callback)
                        loading.postValue(false)
                    }

                    override fun onError(e: Throwable) {
                        onCallError(e)
                        loading.postValue(false)
                    }


                }).addTo(disposables)
        }else

            userDao.getUsers(size, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading.postValue(true) }
                .subscribe({
                    onUsersFetched(it, callback)
                    loading.postValue(false)
                },{
                    onCallError(it)
                    loading.postValue(false)
                }).addTo(disposables)
    }

    private fun onUsersFetched(users: List<UserData>, callback: LoadInitialCallback<UserData>) {
        pageNumber+=size
        empty.postValue(users.isEmpty())
        if(users.isNotEmpty())
            userDao.insert(users)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({},{ it.printStackTrace() }).addTo(disposables)

        users.getOrNull(3)?.invertColor = true
        callback.onResult(users)
    }

    private fun onCallError(throwable: Throwable) {
        throwable.printStackTrace()
        empty.postValue(true)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<UserData>) {
        this.params = params
        this.callback = callback

        if(hasInternet.value == true) {
            repository.getUsers(params.key)
                .doOnSubscribe { loading.postValue(true) }
                .subscribeWith(object : DisposableMaybeObserver<List<UserData>>() {

                    override fun onComplete() {
                        loading.postValue(false)
                    }

                    override fun onSuccess(users: List<UserData>) {
                        onMoreUsersFetched(users, callback)
                        loading.postValue(false)
                    }

                    override fun onError(e: Throwable) {
                        onPaginationError(e)
                        loading.postValue(false)
                    }


                }).addTo(disposables)
        }else
            userDao.getUsers(size, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading.postValue(true) }
                .subscribe({
                    onMoreUsersFetched(it, callback)
                    loading.postValue(false)
                },{
                    onCallError(it)
                    loading.postValue(false)
                }).addTo(disposables)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<UserData>) {}

    override fun getKey(item: UserData): Int = pageNumber

    private fun onMoreUsersFetched(users: List<UserData>, callback: LoadCallback<UserData>) {
        pageNumber+=size
        if(users.isNotEmpty())
            userDao.insert(users)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({},{ it.printStackTrace() }).addTo(disposables)

        users.getOrNull(3)?.invertColor = true
        callback.onResult(users)
    }

    private fun onPaginationError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    fun clear() {
        pageNumber = 0
        disposables.clear()
    }
}
