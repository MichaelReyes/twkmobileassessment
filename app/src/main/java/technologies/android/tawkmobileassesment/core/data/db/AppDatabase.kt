package technologies.android.tawkmobileassesment.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.NoteData
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData

@Database(entities = [ UserData::class, NoteData::class ],
        version = AppDatabase.VERSION, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "tawkassessmentapp.db"
        const val VERSION = 2
    }

    abstract fun userDataDao(): UserDataDao
    abstract fun noteDataDao(): NoteDataDao
}