package technologies.android.tawkmobileassesment.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import technologies.android.tawkmobileassesment.core.di.scope.FragmentScope
import technologies.android.tawkmobileassesment.feature.dashboard.user_details.UserDetailsFragment
import technologies.android.tawkmobileassesment.feature.dashboard.users.UsersFragment

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUsersFragment(): UsersFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsFragment(): UserDetailsFragment

}