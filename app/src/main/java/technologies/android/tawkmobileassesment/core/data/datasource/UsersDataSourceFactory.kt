package technologies.android.tawkmobileassesment.core.data.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.network.repo.UsersRepository
import javax.inject.Inject


class UsersDataSourceFactory  @Inject constructor(
    private val repo: UsersRepository,
    private val userDao: UserDataDao,
    private val loading: MutableLiveData<Boolean>,
    private val empty: MutableLiveData<Boolean>,
    private val hasInternet: MutableLiveData<Boolean>
) : DataSource.Factory<Int, UserData>() {

    val source = MutableLiveData<UsersDataSource>()

    override fun create(): DataSource<Int, UserData> {
        val source = UsersDataSource(repo,userDao, loading, empty, hasInternet)
        this.source.postValue(source)
        return source
    }
}
