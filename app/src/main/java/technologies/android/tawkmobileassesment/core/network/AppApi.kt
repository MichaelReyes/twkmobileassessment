package technologies.android.tawkmobileassesment.core.network

import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData

interface AppApi {

    @GET("users")
    fun getUsers(@Query("since") start: Int): Maybe<List<UserData>>

    @GET("users/{user_login}")
    fun getUserDetails(@Path("user_login") login: String):  Maybe<UserData>

}