
package technologies.android.tawkmobileassesment.core.utility

class Constants {
    companion object{
        //For API integration with oauth2 using this pref key to save access token
        const val PREF_KEY_ACCESS_TOKEN = "_pref_access_token"

        const val PREF_KEY_NETWORK_CONNECTED = "_pref_network_connected"
    }
}