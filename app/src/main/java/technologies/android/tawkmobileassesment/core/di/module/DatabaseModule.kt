package technologies.android.tawkmobileassesment.core.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import technologies.android.tawkmobileassesment.core.data.db.AppDatabase
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.NoteData
import technologies.android.tawkmobileassesment.core.di.scope.AppScope

@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideUserDataDao(appDataBase: AppDatabase): UserDataDao {
        return appDataBase.userDataDao()
    }

    @Provides
    fun provideNoteDataDao(appDataBase: AppDatabase): NoteDataDao {
        return appDataBase.noteDataDao()
    }


}