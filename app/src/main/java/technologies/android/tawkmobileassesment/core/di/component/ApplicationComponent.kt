package technologies.android.tawkmobileassesment.core.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import technologies.android.tawkmobileassesment.core.base.App
import technologies.android.tawkmobileassesment.core.di.module.*
import technologies.android.tawkmobileassesment.core.di.module.vm.ViewModelModule
import technologies.android.tawkmobileassesment.core.di.scope.AppScope


@AppScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        NetworkModule::class,
        DatabaseModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory: AndroidInjector.Factory<App>
}