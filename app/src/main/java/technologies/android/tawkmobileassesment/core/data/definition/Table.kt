package technologies.android.tawkmobileassesment.core.data.definition

@Retention(AnnotationRetention.RUNTIME)
annotation class Table {
    companion object {
        const val USERS = "TBL_USERS"
        const val NOTES = "TBL_NOTES"
    }
}