package technologies.android.tawkmobileassesment.core.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import technologies.android.tawkmobileassesment.core.base.App
import technologies.android.tawkmobileassesment.core.di.scope.AppScope

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(app: App
    ): Context {
        return app
    }

}