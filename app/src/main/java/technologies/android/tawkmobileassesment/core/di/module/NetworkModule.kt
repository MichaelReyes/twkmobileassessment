package technologies.android.tawkmobileassesment.core.di.module

import android.os.SystemClock
import androidx.annotation.NonNull
import com.orhanobut.hawk.Hawk

import java.util.concurrent.TimeUnit


import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import technologies.android.tawkmobileassesment.BuildConfig
import technologies.android.tawkmobileassesment.core.di.scope.AppScope
import technologies.android.tawkmobileassesment.core.network.AppApi
import technologies.android.tawkmobileassesment.core.utility.Constants
import javax.inject.Named

const val BACKEND_BASE_URL = "https://api.github.com/"

@Module
class NetworkModule {

    internal val loggingInterceptor: HttpLoggingInterceptor
        @Provides
        get() = HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        )

    @Provides
    @Named("Backend")
    fun getBackendApiEndpoint(): String {
        return BACKEND_BASE_URL
    }
    @Provides
    @Named("Backend")
    @AppScope
    internal fun provideApiRetrofit(
        @Named("Backend") @NonNull baseUrl: String, client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @AppScope
    internal fun getHttpClient(
        interceptor: HttpLoggingInterceptor
        /*@Named("Backend") authenticator: Authenticator*/
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()

        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1

        builder
            /*.authenticator(authenticator)*/
            .addInterceptor(httpApiInterceptor())
            .dispatcher(dispatcher)
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)

        builder.addInterceptor(interceptor)

        return builder.build()
    }

    @Provides
    @Named("Backend")
    @AppScope
    fun getAuthenticator(): Authenticator {
        return object : Authenticator {
            override fun authenticate(route: Route?, response: Response): Request? {
                return when {
                    response.request.header("Authorization") != null -> null
                    else -> response.request.newBuilder().header(
                        "Authorization",
                        "Bearer " + Hawk.get(Constants.PREF_KEY_ACCESS_TOKEN, "")
                    ).build()

                }
            }
        }
    }

    private fun httpApiInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .header("Content-Type", "application/json")
                    /* Will comment this out since not needed. But this is how I handle access token for API calls
                .header(
                    "Authorization",
                    when {
                        Hawk.get(Constants.PREF_KEY_ACCESS_TOKEN, "").isEmpty() -> "Authorization:"
                        else -> "Bearer " + Hawk.get<String>(Constants.PREF_KEY_ACCESS_TOKEN)
                    }
                )
                    */
                .build()
            SystemClock.sleep(1000)
            chain.proceed(request)
        }
    }

    @Provides
    @Named("Backend")
    @AppScope
    fun getBackendApiService(@Named("Backend") retrofit: Retrofit): AppApi {
        return retrofit.create(AppApi::class.java)
    }

}

