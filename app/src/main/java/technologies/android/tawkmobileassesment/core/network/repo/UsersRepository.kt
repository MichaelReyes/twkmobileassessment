package technologies.android.tawkmobileassesment.core.network.repo

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.network.AppApi
import javax.inject.Inject
import javax.inject.Named

class UsersRepository @Inject constructor(
    @Named("Backend") private val service: AppApi
) {

    fun getUsers(start: Int): Maybe<List<UserData>> {
        return service.getUsers(start).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getUserDetails(login: String): Maybe<UserData>{
        return service.getUserDetails(login).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}