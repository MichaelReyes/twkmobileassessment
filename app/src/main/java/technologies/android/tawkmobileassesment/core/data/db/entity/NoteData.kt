package technologies.android.tawkmobileassesment.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import technologies.android.tawkmobileassesment.core.data.definition.Table

@Entity(tableName = Table.NOTES)
data class NoteData(
    @PrimaryKey
    @ColumnInfo(name = "userId")
    val userId: Int,
    @ColumnInfo(name = "notes")
    val notes: String
)