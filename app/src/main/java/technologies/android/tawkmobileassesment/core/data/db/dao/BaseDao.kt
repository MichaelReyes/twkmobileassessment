package technologies.android.tawkmobileassesment.core.data.db.dao

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(vararg t: T): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(t: List<T>): Completable

    @Update
    fun update(vararg t: T): Completable

    @Delete
    fun delete(vararg t: T): Completable
}