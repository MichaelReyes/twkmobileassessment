package technologies.android.tawkmobileassesment.core.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.data.definition.Table

@Dao
interface UserDataDao: BaseDao<UserData> {

    @Query("SELECT * FROM ${Table.USERS} LIMIT :limit OFFSET :offset")
    fun getUsers(limit: Int, offset: Int): Single<List<UserData>>

    @Query("SELECT * FROM ${Table.USERS} WHERE login = :login")
    fun getUserById(login: String): Single<UserData>
}