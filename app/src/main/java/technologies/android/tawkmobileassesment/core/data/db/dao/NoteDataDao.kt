package technologies.android.tawkmobileassesment.core.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import technologies.android.tawkmobileassesment.core.data.db.entity.NoteData
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.data.definition.Table

@Dao
interface NoteDataDao: BaseDao<NoteData> {

    @Query("SELECT * FROM ${Table.NOTES} WHERE userId = :userId")
    fun getUserNote(userId: Int): Single<NoteData>

}