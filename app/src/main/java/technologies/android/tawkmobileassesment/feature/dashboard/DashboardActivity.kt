package technologies.android.tawkmobileassesment.feature.dashboard

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dashboard.*
import technologies.android.tawkmobileassesment.R
import technologies.android.tawkmobileassesment.core.base.BaseActivity
import technologies.android.tawkmobileassesment.databinding.ActivityDashboardBinding

class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {

    override val layoutRes: Int
        get() = R.layout.activity_dashboard

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
        setToolbar(show = true, showBackButton = false, title = "Users")
    }

}