package technologies.android.tawkmobileassesment.feature.dashboard.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import technologies.android.tawkmobileassesment.core.data.datasource.UsersDataSourceFactory
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.network.repo.UsersRepository
import javax.inject.Inject

class UsersViewModel @Inject constructor(
    private val repo: UsersRepository,
    private val userDao: UserDataDao
): ViewModel() {

    lateinit var sourceFactory: UsersDataSourceFactory

    var hasInternetConnection = MutableLiveData(true)
    var empty = MutableLiveData(true)
    var users: LiveData<PagedList<UserData>> = MutableLiveData()
    var loading = MutableLiveData(false)

    fun getUsers(){
        val config = PagedList.Config.Builder()
            .setPageSize(5)
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(5)
            .build()

        sourceFactory = UsersDataSourceFactory(repo,userDao, loading, empty, hasInternetConnection)

        users = LivePagedListBuilder(sourceFactory, config).build()
    }

    override fun onCleared() {
        super.onCleared()
    }
}