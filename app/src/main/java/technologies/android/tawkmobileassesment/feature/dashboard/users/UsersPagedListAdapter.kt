package technologies.android.tawkmobileassesment.feature.dashboard.users

import android.content.Context
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.room.EmptyResultSetException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import technologies.android.tawkmobileassesment.R
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.utility.Constants
import technologies.android.tawkmobileassesment.databinding.ItemUserBinding

class UsersPagedListAdapter constructor(
    @NonNull val diffCallback: DiffUtil.ItemCallback<UserData>,
    val notesDao: NoteDataDao
): PagedListAdapter<UserData, RecyclerView.ViewHolder>(diffCallback)  {

    private val colorFilterNegative = floatArrayOf(
        -1.0f, 0f, 0f, 0f, 255f,
        0f, -1.0f, 0f, 0f, 255f,
        0f, 0f, -1.0f, 0f, 255f,
        0f, 0f, 0f, 1.0f, 0f
    )

    val disposable = CompositeDisposable()

    internal var clickListener: (UserData) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemUserBinding>(LayoutInflater.from(parent.context),
            R.layout.item_user, parent, false)
        return Holder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as Holder
        viewHolder.bind(getItem(position)!!)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding: ItemUserBinding? = DataBindingUtil.bind(itemView)

        internal fun bind(user: UserData) {
            binding?.apply {
                this.user = user
                itemView.setOnClickListener { clickListener(user) }
                if(user.invertColor)
                    ivImage.setImageUrlWithFilter(user.avatarUrl, colorFilterNegative)
                else
                    ivImage.setImageUrl(user.avatarUrl)

                /*
                if(user.invertColor) {
                    ivImage.drawable.colorFilter = ColorMatrixColorFilter(colorFilterNegative)
                    ivImage.invalidate()
                }
                */

                executePendingBindings()

                notesDao.getUserNote(user.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        user.notes = it.notes
                        notifyItemChanged(adapterPosition)
                    },{
                       it.printStackTrace()
                    }).addTo(disposable)
            }
        }
    }
}

