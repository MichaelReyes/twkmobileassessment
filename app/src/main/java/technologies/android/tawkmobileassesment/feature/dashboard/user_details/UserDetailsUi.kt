package technologies.android.tawkmobileassesment.feature.dashboard.user_details

import technologies.android.tawkmobileassesment.core.data.db.entity.UserData


data class UserDetailsUi(
    var loading: Boolean = false,
    var message: Pair<String, Boolean>? =  null,
    var notes: String =  "",
    var notesValid: Boolean =  false,
    var savingNotes: Boolean =  false
)
