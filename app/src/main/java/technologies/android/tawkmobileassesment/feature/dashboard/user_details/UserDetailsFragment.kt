package technologies.android.tawkmobileassesment.feature.dashboard.user_details

import android.os.Bundle
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user_details.*
import kotlinx.android.synthetic.main.banner_no_network.*
import technologies.android.tawkmobileassesment.R
import technologies.android.tawkmobileassesment.core.base.App
import technologies.android.tawkmobileassesment.core.base.BaseActivity
import technologies.android.tawkmobileassesment.core.base.BaseFragment
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.extension.observe
import technologies.android.tawkmobileassesment.core.extension.viewModel
import technologies.android.tawkmobileassesment.databinding.FragmentUserDetailsBinding
import javax.inject.Inject

class UserDetailsFragment : BaseFragment<FragmentUserDetailsBinding>() {

    @Inject
    lateinit var vm: UserDetailsViewModel

    private val disposables = CompositeDisposable()
    private var userData: UserData? = null

    override val layoutRes = R.layout.fragment_user_details

    override fun onCreated(savedInstance: Bundle?) {

        initObserver()
        checkArgs()
    }

    private fun initObserver() {
        vm = viewModel(viewModelFactory){
            observe(ui,::handleUiUpdates)
            observe(userDetails, ::handleDetailsUpdates)
        }

        binding.lifecycleOwner = this
        binding.vm = vm

        activity?.apply {
            (application as App).internetConnectionStream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    clNoNetwork.visibility = if(it) View.GONE else View.VISIBLE
                    vm.hasInternetConnection.value = it
                    vm.setUserDetails(userData)
                }.addTo(disposables)
        }
    }

    private fun checkArgs() {
        arguments?.apply {
            if(containsKey(ARGS_USER))
                userData = gson.fromJson(getString(ARGS_USER), UserData::class.java)
            /*
                vm.setUserDetails(gson.fromJson(getString(ARGS_USER), UserData::class.java))
            else
                vm.setUserDetails(null)
            */
        }
    }

    private fun handleDetailsUpdates(userData: UserData?) {
        userData?.apply {
            if(avatarUrl.isNotEmpty())
                ivAvatar.setImageUrlRound(avatarUrl)
        }
    }

    private fun handleUiUpdates(userDetailsUi: UserDetailsUi?) {
        userDetailsUi?.apply {
            (activity as? BaseActivity<*>)?.showLoading(loading)

            message?.apply {
                showMessage(this.first, this.second)
                vm.setMessage(null)
            }
        }
    }

    companion object{
        const val ARGS_USER = "_args_user"
    }

}