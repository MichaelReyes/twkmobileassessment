package technologies.android.tawkmobileassesment.feature.dashboard.user_details

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user_details.*
import kotlinx.android.synthetic.main.banner_no_network.*
import technologies.android.tawkmobileassesment.R
import technologies.android.tawkmobileassesment.core.base.App
import technologies.android.tawkmobileassesment.core.base.BaseActivity
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.extension.observe
import technologies.android.tawkmobileassesment.core.extension.viewModel
import technologies.android.tawkmobileassesment.databinding.ActivityUserDetailsBinding
import javax.inject.Inject

class UserDetailsActivity : BaseActivity<ActivityUserDetailsBinding>() {

    @Inject
    lateinit var vm: UserDetailsViewModel

    private val disposables = CompositeDisposable()

    override val layoutRes: Int
        get() = R.layout.activity_user_details

    var userData: UserData? = null

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)

        setToolbar(show = true, showBackButton = true)

        initObserver()
        checkExtras()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun checkExtras() {
        intent.extras?.apply {
            if(containsKey(EXTRA_USER))
                userData = gson.fromJson(getString(EXTRA_USER), UserData::class.java)
                //vm.setUserDetails(Gson().fromJson(getString(EXTRA_USER), UserData::class.java))
        }
    }

    private fun initObserver() {
        vm = viewModel(viewModelFactory){
            observe(ui,::handleUiUpdates)
            observe(userDetails, ::handleDetailsUpdates)
        }
        binding.lifecycleOwner = this
        binding.vm = vm

        (application as App).internetConnectionStream.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                clNoNetwork.visibility = if(it) View.GONE else View.VISIBLE
                vm.hasInternetConnection.value = it
                vm.setUserDetails(userData)
            }.addTo(disposables)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    private fun handleDetailsUpdates(userData: UserData?) {
        userData?.apply {

            this@UserDetailsActivity.userData = this

            name?.let { n ->
                setToolbar(show = true, showBackButton = true, title = n)
            }

            if(avatarUrl.isNotEmpty())
                ivAvatar.setImageUrlRound(avatarUrl)
        }
    }

    private fun handleUiUpdates(userDetailsUi: UserDetailsUi?) {
        userDetailsUi?.apply {
            showLoading(loading)

            message?.apply {
                showMessage(this.first, this.second)
                vm.setMessage(null)
            }
        }
    }

    companion object{
        const val ACTIVITY_CODE = 1113
        const val EXTRA_USER = "_extra_user"
    }
}