package technologies.android.tawkmobileassesment.feature.dashboard.user_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.room.EmptyResultSetException
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import technologies.android.tawkmobileassesment.core.data.datasource.UsersDataSourceFactory
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.db.dao.UserDataDao
import technologies.android.tawkmobileassesment.core.data.db.entity.NoteData
import technologies.android.tawkmobileassesment.core.data.db.entity.UserData
import technologies.android.tawkmobileassesment.core.extension.configureInterceptorWithEmpty
import technologies.android.tawkmobileassesment.core.network.repo.UsersRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val repo: UsersRepository,
    private val userDao: UserDataDao,
    private val noteDao: NoteDataDao
) : ViewModel() {

    private val disposable = CompositeDisposable()

    val userDetails = MutableLiveData<UserData>()

    var hasInternetConnection = MutableLiveData(true)

    private val autoCompletePublishSubjectNotes = PublishSubject.create<String>()

    init {
        autoCompletePublishSubjectNotes.configureInterceptorWithEmpty(0)
            .subscribe { result -> filteredNotes(result) }.addTo(disposable)
    }

    fun onNotesChange(text: CharSequence) {
        autoCompletePublishSubjectNotes.onNext(text.toString())
    }

    private fun filteredNotes(result: String) {
        ui.value?.apply {
            notes = result
            notesValid = result.isNotEmpty()
            ui.value = this
        }
    }

    fun setUserDetails(details: UserData?) {
        details?.apply {
            if (this.name.isNullOrEmpty())
                getUserDetails(login)
            getUserNotes(this.id)
            userDetails.value = details
        }
    }

    fun getUserDetails(login: String) {

        if (hasInternetConnection.value == true) {
            repo.getUserDetails(login)
                .flatMap {
                    userDao.insert(it)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .andThen(Maybe.just(it))
                }
                .doOnSubscribe { setLoading(true) }
                .subscribe({
                    setLoading(false)
                    userDetails.value = it
                    //setUserDetails(it)
                }, {
                    setLoading(false)
                    setMessage(Pair(it.localizedMessage ?: "", false))
                }).addTo(disposable)
        } else
            userDao.getUserById(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { setLoading(true) }
                .subscribe({
                    setLoading(false)
                    //setUserDetails(it)
                    userDetails.value = it
                }, {
                    setLoading(false)
                    setMessage(Pair(it.localizedMessage ?: "", false))
                }).addTo(disposable)
    }

    fun getUserNotes(userId: Int){
        noteDao.getUserNote(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { setLoading(true) }
            .subscribe({
                onNotesChange(it.notes)
                setLoading(false)
            },{
                if(it !is EmptyResultSetException)
                    setMessage(Pair(it.localizedMessage ?: "", false))
                setLoading(false)
            }).addTo(disposable)
    }

    var ui = MutableLiveData(UserDetailsUi())

    private fun setLoading(value: Boolean) {
        ui.value?.apply {
            loading = value
            ui.value = this
        }
    }

    fun setMessage(value: Pair<String, Boolean>?) {
        ui.value?.apply {
            message = value
            ui.value = this
        }
    }

    fun setSavingNotes(value: Boolean) {
        ui.value?.apply {
            savingNotes = value
            ui.value = this
        }
    }

    fun saveNote() {
        ui.value?.let { ui ->
            userDetails.value?.let { details ->
                noteDao.insert(
                    NoteData(details.id, ui.notes)
                ).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { setLoading(true) }
                    .subscribe({
                        setMessage(Pair("Notes saved successfully!", true))
                        setLoading(false)
                        setSavingNotes(false)
                    },{
                        setLoading(false)
                        setMessage(Pair(it.localizedMessage ?: "", false))
                        setSavingNotes(false)
                    }).addTo(disposable)
            }
            setSavingNotes(true)
        }
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }
}