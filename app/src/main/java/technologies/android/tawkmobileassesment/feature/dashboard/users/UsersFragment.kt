package technologies.android.tawkmobileassesment.feature.dashboard.users

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.banner_no_network.*
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.list_users.*
import technologies.android.tawkmobileassesment.R
import technologies.android.tawkmobileassesment.core.base.App
import technologies.android.tawkmobileassesment.core.base.BaseActivity
import technologies.android.tawkmobileassesment.core.base.BaseFragment
import technologies.android.tawkmobileassesment.core.data.db.dao.NoteDataDao
import technologies.android.tawkmobileassesment.core.data.diffutil.UsersDiffUtilItemCallback
import technologies.android.tawkmobileassesment.core.extension.observe
import technologies.android.tawkmobileassesment.core.extension.viewModel
import technologies.android.tawkmobileassesment.databinding.FragmentUsersBinding
import technologies.android.tawkmobileassesment.feature.dashboard.user_details.UserDetailsActivity
import technologies.android.tawkmobileassesment.feature.dashboard.user_details.UserDetailsFragment
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class UsersFragment : BaseFragment<FragmentUsersBinding>() {

    private val disposables = CompositeDisposable()

    lateinit var adapter: UsersPagedListAdapter

    @Inject
    lateinit var notesDataDao: NoteDataDao

    @Inject
    lateinit var vm: UsersViewModel

    private var twoPane = false
    private var listSkeleton: Skeleton? = null

    override val layoutRes = R.layout.fragment_users

    override fun onCreated(savedInstance: Bundle?) {
        twoPane = baseView.findViewById<ViewGroup>(R.id.nav_host_fragment) != null

        initViews()
        initObservers()
    }

    private fun initObservers() {
        vm = viewModel(viewModelFactory) {
            getUsers()
            observe(users) { adapter.submitList(it) }
            observe(loading) {
                it?.apply {
                    //(activity as? BaseActivity<*>)?.showLoading(it)
                    loader.visibility = if (this) View.VISIBLE else View.GONE
                    if (this) {
                        if (adapter.itemCount == 0 && listSkeleton?.isSkeleton() != true)
                            listSkeleton?.showSkeleton()
                    } else
                        if (listSkeleton?.isSkeleton() == true)
                            listSkeleton?.showOriginal()
                }
            }
            observe(empty, {
                it?.apply {
                    tvEmpty?.visibility = if(this) View.VISIBLE else View.GONE
                }
            })
        }

        activity?.apply {
            (application as App).internetConnectionStream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    clNoNetwork.visibility = if(it) View.GONE else View.VISIBLE
                    vm.hasInternetConnection.value = it
                    vm.sourceFactory.source.value?.invalidate()
                }.addTo(disposables)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }

    private fun initViews() {
        adapter = UsersPagedListAdapter(UsersDiffUtilItemCallback(), notesDataDao)
        rvUsers.adapter = adapter

        adapter.clickListener = {
            if (twoPane) {
                val navHostFragment =
                    childFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

                navHostFragment.navController.navigate(
                    R.id.userDetailsFragment,
                    bundleOf(Pair(UserDetailsFragment.ARGS_USER, gson.toJson(it)))
                )
            } else {
                goToActivity(
                    requireActivity(), UserDetailsActivity::class.java, false,
                    bundleOf(
                        Pair(UserDetailsActivity.EXTRA_USER, gson.toJson(it))
                    ), true, UserDetailsActivity.ACTIVITY_CODE
                )
            }
        }

        listSkeleton = rvUsers.applySkeleton(R.layout.item_user, 10)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == UserDetailsActivity.ACTIVITY_CODE)
            adapter.notifyDataSetChanged()
    }
}